/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis;

import java.net.URL;
import java.util.Objects;

/**
 *
 * @author david
 */
public final class OntologyTerm extends AbstractEntity {

    private static final String FIELD_ID = "id";
    private static final String FIELD_LABEL = "label";
    private static final String FIELD_URI = "uri";
    private static final String FIELD_ONTOLOGY = "ontology";
    private static final String FIELD_DESCRIPTION = "description";

    private static final String[] FIELDS = {FIELD_ID, FIELD_LABEL, FIELD_URI,
        FIELD_ONTOLOGY, FIELD_DESCRIPTION};
    private String label;
    private URL uri;
    private String ontology;
    private String descripton;
    private String id;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OntologyTerm other = (OntologyTerm) obj;
        return Objects.equals(this.id, other.id);
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return id;
            case FIELD_LABEL:
                return label;
            case FIELD_URI:
                return uri != null ? uri.toString() : "";
            case FIELD_ONTOLOGY:
                return ontology;
            case FIELD_DESCRIPTION:
                return descripton;
            default:
                throw new RuntimeException("illegal field");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getOntology() {
        return ontology;
    }

    public void setOntology(String ontology) {
        this.ontology = ontology;
    }

    public URL getUri() {
        return uri;
    }

    public void setUri(URL uri) {
        this.uri = uri;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(id);
        return hash;
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }

    @Override
    public String toString() {
        return id;
    }
}
