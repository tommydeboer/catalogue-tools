/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis;

import java.util.Collection;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author david
 */
public abstract class AbstractEntity {

    public abstract String getFieldValue(String name);
    public abstract String[] getFieldNames();

    protected String getStringValue(final String value, final String defaultValue) {
        return (StringUtils.isBlank(value)) ? defaultValue : value;
    }

    protected<T> String convertCollectionToString(final Collection<T> list) {
        if (list == null || list.isEmpty()) {
            return "";
        }
        final String result = list.stream().filter(e -> e != null).map(e -> e.toString())
                .collect(Collectors.joining(","));
        return result == null ? "" : result;
    }
}
