/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis.valuesets;

/**
 *
 * @author david
 */
public enum OmicsData {

    GENOMICS,
    TRANSCRIPTOMICS,
    PROTEOMICS,
    METABOLOMICS,
    OTHER,
    NAV;
}
