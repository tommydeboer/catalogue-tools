/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis;

import nl.allthingsdigital.catalogue.tools.miabis.valuesets.OmicsData;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Gender;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Material;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.DataCategory;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.CollectionType;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.AgeUnit;
import java.net.URL;
import java.util.Set;

/**
 *
 * @author david
 */
public class SampleCollection extends AbstractEntity {

    private static final String FIELD_ID = "id";
    private static final String FIELD_BIOBANKS = "biobanks";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_ACRONYM = "acronym";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_WEBPAGE = "website";
    private static final String FIELD_CONTACT_PERSON = "contact_person";
    private static final String FIELD_PRINCIPAL_INVESTIGATORS = "principal_investigators";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_DATA_CATEGORIES = "data_categories";
    private static final String FIELD_MATERIALS = "materials";
    private static final String FIELD_OMICS = "omics";
    private static final String FIELD_SEX = "sex";
    private static final String FIELD_AGE_LOW = "age_low";
    private static final String FIELD_AGE_HIGH = "age_high";
    private static final String FIELD_AGE_UNIT = "age_unit";
    private static final String FIELD_NUMBER_OF_DONORS = "numberOfDonors";
    private static final String FIELD_DISEASE = "disease";
    private static final String FIELD_INSTITUTES = "institutes";
    private static final String FIELD_PUBLICATIONS = "publications";

    private static final String[] FIELDS = {
        FIELD_ID, FIELD_BIOBANKS, FIELD_NAME, FIELD_ACRONYM, FIELD_DESCRIPTION,
        FIELD_TYPE, FIELD_DISEASE, FIELD_MATERIALS, FIELD_DATA_CATEGORIES,
        FIELD_OMICS, FIELD_SEX, FIELD_NUMBER_OF_DONORS, FIELD_AGE_LOW,
        FIELD_AGE_HIGH, FIELD_AGE_UNIT, FIELD_WEBPAGE, FIELD_CONTACT_PERSON,
        FIELD_PRINCIPAL_INVESTIGATORS, FIELD_INSTITUTES, FIELD_PUBLICATIONS
    };

    private String id;
    private Set<Biobank> biobanks;
    private String name;
    private String acronym;
    private String description;
    private URL webpage;
    private Person contactPerson;
    private Set<Person> principalInvestigators;
    private Set<CollectionType> types;
    private Set<DataCategory> dataCategories;
    private Set<Material> materials;
    private Set<OmicsData> omics;
    private Set<Gender> sex;
    private Set<OntologyTerm> diseases;
    private Integer ageLow;
    private Integer ageHigh;
    private AgeUnit ageUnit;
    private Integer numberOfDonors;
    private Set<Organisation> institutes;
    private Set<Publication> publications;

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setAgeHigh(Integer ageHigh) {
        this.ageHigh = ageHigh;
    }

    public Integer getAgeHigh() {
        return ageHigh;
    }

    public void setAgeLow(Integer ageLow) {
        this.ageLow = ageLow;
    }

    public Integer getAgeLow() {
        return ageLow;
    }

    public AgeUnit getAgeUnit() {
        return ageUnit;
    }

    public void setAgeUnit(AgeUnit ageUnit) {
        this.ageUnit = ageUnit;
    }

    public Set<Biobank> getBiobanks() {
        return biobanks;
    }

    public void setBiobanks(Set<Biobank> biobanks) {
        this.biobanks = biobanks;
    }

    public Person getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(Person contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Set<DataCategory> getDataCategories() {
        return dataCategories;
    }

    public void setDataCategories(Set<DataCategory> dataCategories) {
        this.dataCategories = dataCategories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<OntologyTerm> getDiseases() {
        return diseases;
    }

    public void setDiseases(Set<OntologyTerm> diseases) {
        this.diseases = diseases;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<Organisation> getInstitutes() {
        return institutes;
    }

    public void setInstitutes(Set<Organisation> institutes) {
        this.institutes = institutes;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfDonors(Integer numberOfDonors) {
        this.numberOfDonors = numberOfDonors;
    }

    public Integer getNumberOfDonors() {
        return numberOfDonors;
    }

    public Set<OmicsData> getOmics() {
        return omics;
    }

    public void setOmics(Set<OmicsData> omics) {
        this.omics = omics;
    }

    public Set<Person> getPrincipalInvestigators() {
        return principalInvestigators;
    }

    public void setPrincipalInvestigators(Set<Person> principalInvestigators) {
        this.principalInvestigators = principalInvestigators;
    }

    public Set<Publication> getPublications() {
        return publications;
    }

    public void setPublications(Set<Publication> publications) {
        this.publications = publications;
    }

    public Set<Gender> getSex() {
        return sex;
    }

    public void setSex(Set<Gender> sex) {
        this.sex = sex;
    }

    public Set<CollectionType> getTypes() {
        return types;
    }

    public void setTypes(Set<CollectionType> types) {
        this.types = types;
    }

    public URL getWebpage() {
        return webpage;
    }

    public void setWebpage(URL webpage) {
        this.webpage = webpage;
    }

    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return getStringValue(id, "");
            case FIELD_BIOBANKS:
                return convertCollectionToString(biobanks);
            case FIELD_NAME:
                return getStringValue(name, "N/A");
            case FIELD_ACRONYM:
                return getStringValue(acronym, "");
            case FIELD_DESCRIPTION:
                return getStringValue(description, "");
            case FIELD_WEBPAGE:
                return webpage != null ? webpage.toString() : "";
            case FIELD_CONTACT_PERSON:
                return contactPerson != null ? contactPerson.getId() : "";
            case FIELD_PRINCIPAL_INVESTIGATORS:
                return convertCollectionToString(principalInvestigators);
            case FIELD_TYPE:
                return convertCollectionToString(types);
            case FIELD_DATA_CATEGORIES:
                return convertCollectionToString(dataCategories);
            case FIELD_MATERIALS:
                return convertCollectionToString(materials);
            case FIELD_OMICS:
                return convertCollectionToString(omics);
            case FIELD_SEX:
                return convertCollectionToString(sex);
            case FIELD_AGE_LOW:
                return ageLow != null ? Integer.toString(ageLow) : "";
            case FIELD_AGE_HIGH:
                return ageHigh != null ? Integer.toString(ageHigh) : "";
            case FIELD_AGE_UNIT:
                return ageUnit != null ? ageUnit.name() : "";
            case FIELD_NUMBER_OF_DONORS:
                return numberOfDonors != null ? Integer.toString(numberOfDonors) : "";
            case FIELD_DISEASE:
                return convertCollectionToString(diseases);
            case FIELD_INSTITUTES:
                return convertCollectionToString(institutes);
            case FIELD_PUBLICATIONS:
                return convertCollectionToString(publications);
            default:
                throw new RuntimeException("illegal field");
        }
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }
}
