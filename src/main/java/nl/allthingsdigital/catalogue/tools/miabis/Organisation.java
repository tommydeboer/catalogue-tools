/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.allthingsdigital.catalogue.tools.miabis;

import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Country;
import java.util.Objects;

/**
 *
 * @author david
 */
public class Organisation extends AbstractEntity {
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_ADDRESS = "address";
    private static final String FIELD_ZIP = "zip";
    private static final String FIELD_CITY = "city";
    private static final String FIELD_COUNTRY = "country";
    private static final String[] FIELDS = { FIELD_ID, FIELD_NAME,
        FIELD_ADDRESS, FIELD_ZIP, FIELD_CITY, FIELD_COUNTRY};
    private String id;
    private String name;
    private String address;
    private String zip;
    private String city;
    private Country country;    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Organisation other = (Organisation) obj;
        return Objects.equals(this.name, other.name);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(final Country country) {
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }
        
    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return id != null ? id : "";
            case FIELD_NAME:
                return name != null ? name : "";
            case FIELD_ADDRESS:
                return address != null ? address : "";
            case FIELD_ZIP:
                return zip != null ? zip : "";
            case FIELD_CITY:
                return city != null ? city : "";
            case FIELD_COUNTRY:
                return country != null ? country.getId() : "";
            default:
                throw new RuntimeException("illegal field");
        }
    }
    @Override
    public String toString() {
        return id != null ? id : "";
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }
}
