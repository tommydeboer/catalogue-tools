/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.miabis;

import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Country;
import java.net.URL;
import java.util.List;

/**
 *
 * @author david
 */
public class Biobank extends AbstractEntity {
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_ACRONYM = "acronym";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_WEBSITE = "website";
    private static final String FIELD_ORGANISATION = "juristic_person";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_CONTACT_PERSON = "contact_person";
    private static final String FIELD_PRINCIPLE_INVESTIGATORS = "principle_investigators";

    private static final String[] FIELDS = {FIELD_ID, FIELD_NAME, FIELD_ACRONYM,
        FIELD_DESCRIPTION, FIELD_WEBSITE, FIELD_ORGANISATION, FIELD_COUNTRY,
        FIELD_CONTACT_PERSON, FIELD_PRINCIPLE_INVESTIGATORS};
    
    private String id;
    private String name;
    private String acronym;
    private String description;
    private URL website;
    private Organisation organisation;
    private Country country;
    private Person contactPerson;
    private List<Person> principalInvestigators;

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Person getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(Person contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPrincipalInvestigators() {
        return principalInvestigators;
    }

    public void setPrincipalInvestigators(List<Person> principalInvestigators) {
        this.principalInvestigators = principalInvestigators;
    }

    public URL getWebsite() {
        return website;
    }

    public void setWebsite(URL website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return id != null ? id : "";
    }

    @Override
    public String[] getFieldNames() {
        return FIELDS;
    }

    @Override
    public String getFieldValue(final String fieldName) {
        switch (fieldName) {
            case FIELD_ID:
                return getStringValue(id, "");
            case FIELD_NAME:
                return getStringValue(name, "");
            case FIELD_ACRONYM:
                return getStringValue(acronym, "");
            case FIELD_DESCRIPTION:
                return getStringValue(description, "");
            case FIELD_WEBSITE:
                return website == null ? "" : website.toString();
            case FIELD_ORGANISATION:
                return organisation == null ? "" : organisation.toString();
            case FIELD_COUNTRY:
                return country == null ? "" : country.toString();
            case FIELD_CONTACT_PERSON:
                return contactPerson == null ? "" : contactPerson.toString();
            case FIELD_PRINCIPLE_INVESTIGATORS:
                return convertCollectionToString(principalInvestigators);
            default:
                throw new RuntimeException("illegal field");
        }
    }

}
