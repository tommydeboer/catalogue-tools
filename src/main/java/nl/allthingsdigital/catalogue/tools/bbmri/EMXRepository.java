/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import nl.allthingsdigital.catalogue.tools.miabis.AbstractEntity;
import nl.allthingsdigital.catalogue.tools.miabis.Organisation;
import nl.allthingsdigital.catalogue.tools.miabis.Person;
import nl.allthingsdigital.catalogue.tools.miabis.Publication;
import nl.allthingsdigital.catalogue.tools.miabis.SampleCollection;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Component
public class EMXRepository implements Repository {

    private static short getCell(Row row) {
        return row.getLastCellNum() == -1 ? 0 : row.getLastCellNum();
    }

    @Override
    public void write(final List<SampleCollection> collections, final Path outfile) throws IOException {
        try {
            final Workbook wb;
            wb = WorkbookFactory.create(ClassLoader.getSystemResourceAsStream("emx-template.xlsx"));
            Set<Person> persons = new HashSet<>();
            Set<Organisation> organisations = new HashSet<>();
            Set<Publication> publications = new HashSet<>();
            collections.forEach((SampleCollection collection) -> {
                if (collection.getContactPerson() != null) {
                    persons.add(collection.getContactPerson());
                    if (collection.getContactPerson().getOrganisation() != null) {
                        organisations.add(collection.getContactPerson().getOrganisation());
                    }
                }
                Optional.ofNullable(collection.getPrincipalInvestigators()).ifPresent(S -> S.forEach((Person person) -> {
                    persons.add(person);
                    Optional.ofNullable(person.getOrganisation()).ifPresent(O -> organisations.add(O));
                }));
                collection.getInstitutes().forEach(organisations::add);
                Optional.ofNullable(collection.getPublications()).ifPresent(S -> S.forEach(publications::add));
            });
            writeSheet(organisations, replaceSheet("juristic_persons", wb));
            writeSheet(persons, replaceSheet("persons", wb));
            writeSheet(collections, replaceSheet("sample_collections", wb));
            writeSheet(publications, replaceSheet("publications", wb));
            wb.write(new FileOutputStream(outfile.toFile()));
        } catch (final InvalidFormatException ex) {
            throw new RuntimeException("Excepting while loading template resource.", ex);
        }
    }

    private Sheet replaceSheet(final String name, final Workbook wb) {
        Sheet sheet = wb.getSheet(name);
        if (sheet != null) {
            wb.removeSheetAt(wb.getSheetIndex(sheet));
        }
        return wb.createSheet(name);
    }

    private <E extends AbstractEntity> void writeHeader(final Sheet sheet, final E e) {
        Row row = sheet.createRow(0);
        for (String fieldName : e.getFieldNames()) {
            Cell cell = row.createCell(getCell(row));
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(fieldName);
        }
    }

    private <E extends AbstractEntity> void writeRecord(final Sheet sheet, final E entity) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        for (String field : entity.getFieldNames()) {
            Cell cell = row.createCell(getCell(row));
            String value = entity.getFieldValue(field);
            if (StringUtils.isBlank(value)) {
                cell.setCellType(Cell.CELL_TYPE_BLANK);
            } else {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(value);
            }
        }
    }

    private <E extends AbstractEntity> void writeSheet(final Collection<E> entities, final Sheet sheet) {
        boolean haveHeader = false;
        for (E entity : entities) {
            if (entity != null) {
                if (!haveHeader) {
                    writeHeader(sheet, entity);
                    haveHeader = true;
                }
                writeRecord(sheet, entity);
            }
        }
    }

}
