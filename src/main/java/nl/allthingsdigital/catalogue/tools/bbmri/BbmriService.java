/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import nl.allthingsdigital.catalogue.tools.miabis.Biobank;
import nl.allthingsdigital.catalogue.tools.miabis.OntologyTerm;
import nl.allthingsdigital.catalogue.tools.miabis.Organisation;
import nl.allthingsdigital.catalogue.tools.miabis.Person;
import nl.allthingsdigital.catalogue.tools.miabis.Publication;
import nl.allthingsdigital.catalogue.tools.miabis.SampleCollection;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.AgeUnit;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.CollectionType;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Country;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.DataCategory;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Gender;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Material;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.OmicsData;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author david
 */
@Component
public class BbmriService implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(BbmriService.class);

    @Autowired
    private DataMapperService dataMapper;

    @Autowired
    private DataSource ds;

    private transient JdbcTemplate jdbc;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (ds == null) {
            throw new RuntimeException("DataSource cannot be null.");
        }
        jdbc = new JdbcTemplate(ds);
    }

    public List<SampleCollection> getSampleCollections() throws DataAccessException {
        List<SampleCollection> collections = jdbc.query("select * from Biobank where Cohort <> 'TEST' and Cohort not like 'Parel%';", (ResultSet rs, int rowNum) -> {
            SampleCollection sc = new SampleCollection();
            final String acronym = rs.getString("Acronym");
            final String name = rs.getString("Cohort");
            if (!name.startsWith("NOT AVAILABLE")
                    && !name.startsWith("Radboud Biobank")
                    && !name.matches("^Nijmegen\\d?$")) {
                if (name.startsWith("POLYGENE")) {
                    sc.setName("Polygene");
                } else {
                    sc.setName(name);
                }
            }
            if (name.startsWith("Radboud Biobank")) {
                Biobank biobank = new Biobank();
                biobank.setId("RBB");
                sc.setBiobanks(asSet(biobank));
            } else if (name.equalsIgnoreCase("Centrale Biobank (CBB) UMC Utrecht")) {
                Biobank biobank = new Biobank();
                biobank.setId("CBB");
                sc.setBiobanks(asSet(biobank));
            }
            sc.setAcronym(acronym);
            sc.setAgeUnit(AgeUnit.YEAR);
            sc.setDataCategories(asSet(DataCategory.BIOLOGICAL_SAMPLES));
            sc.setDescription(rs.getString("GeneralComments"));
            sc.setId(Integer.toString(rs.getInt("id")));
            sc.setNumberOfDonors(normalizeNumber(rs.getString("PanelSize")));
            if (StringUtils.isNotBlank(rs.getString("GwaDataNum"))
                    || StringUtils.isNotBlank(rs.getString("GwaPlatform"))) {
                sc.setOmics(asSet(OmicsData.GENOMICS));
            } else {
                sc.setOmics(asSet(OmicsData.NAV));
            }
            if (StringUtils.isNotBlank(rs.getString("Publications"))) {
                Publication publication = new Publication();
                publication.setId(sc.getId());
                publication.setDescripton(rs.getString("Publications"));
                sc.setPublications(asSet(publication));
            } else {
                sc.setPublications(new HashSet<>());
            }
            sc.setSex(asSet(Gender.UNKNOWN));
            Set<OntologyTerm> topics = getTopics(sc);
            Set<Material> materialTypes = getMaterialTypes(sc);
            sc.setMaterials(materialTypes);
            sc.setTypes(asSet(getCollectionType(sc)));
            sc.setDiseases(getDiseaseTypes(topics));
            Set<Person> principalInvestigators = getPersons(sc);
            sc.setPrincipalInvestigators(principalInvestigators);
            principalInvestigators.stream().findFirst().ifPresent(sc::setContactPerson);
            sc.setInstitutes(getInstitutes(sc));
            return sc;
        });
        collections.addAll(getParels());
        return collections;
    }

    private Set<SampleCollection> getParels() {
        Set<SampleCollection> parels = new HashSet<>();
        Set<DataCategory> data = asSet(Arrays.asList(DataCategory.BIOLOGICAL_SAMPLES, DataCategory.MEDICAL_RECORDS));
        Set<Gender> genders = asSet(Arrays.asList(Gender.FEMALE, Gender.MALE));
        SampleCollection parel = new SampleCollection();
        parel.setId(String.valueOf(115));
        Set<Organisation> organisations = getInstitutes(parel);
        Set<OntologyTerm> diseases = new HashSet<>();
        OntologyTerm ni = new OntologyTerm();
        ni.setId("NI");
        diseases.add(ni);
        Person person = personFactory("info@parelsnoer.org");
        try {
            parel = new SampleCollection();
            parel.setId(String.valueOf(175));
            parel.setAcronym("Parel CVA");
            parel.setName("Parel Cerebro vasculair accident");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Cerebro-vasculair-accident-CVA"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to improve treatment"
                    + " possibilities and to prevent new infarctions and"
                    + " haemorrhages for 120,000 patients living with the"
                    + " consequences of a stroke.");
            parel.setMaterials(asSet(Arrays.asList(Material.PLASMA, Material.DNA, Material.SERUM)));
            parel.setNumberOfDonors(4542);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setContactPerson(person);
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(176));
            parel.setName("Parel CONCOR");
            parel.setAcronym("CONCOR");
            parel.setWebpage(new URL("http://www.concor.net/"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to better understand the"
                    + " long-term complications and genetic causes associated"
                    + " with congenital heart defects. An estimated 50,000"
                    + " people in the Netherlands are living with such a"
                    + " condition, half of them aged over 18.\n"
                    + " doel: oorzaken en late uitkomsten van aangeboren"
                    + " hartafwijkingen. Van 15.000"
                    + " patienten klinische gegevens en toestemming voor"
                    + " DNA bepaling.");
            parel.setMaterials(asSet(Material.DNA));
            parel.setNumberOfDonors(5400);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setContactPerson(person);
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(177));
            parel.setName("Parel Diabetes");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Diabetes"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to improve front-line care in"
                    + " anticipation of a sharp rise in the number of diabetes"
                    + " patients from the current figure of about 600,000.");
            parel.setMaterials(asSet(Arrays.asList(Material.PLASMA,
                    Material.DNA, Material.SERUM, Material.URINE)));
            parel.setNumberOfDonors(6014);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setContactPerson(person);
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(178));
            parel.setName("Parel Erfelijke darmkanker");
            parel.setAcronym("Parel BED");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Erfelijke-darmkanker"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("To improve prevention, treatment and the"
                    + " quality of care for patients with some form of"
                    + " hereditary colorectal cancer, who currently number"
                    + " approximately 4000.");
            parel.setMaterials(asSet(Arrays.asList(Material.DNA, Material.PLASMA,
                    Material.TISSUE_PARAFFIN_EMBEDDED, Material.SERUM)));
            parel.setNumberOfDonors(1650);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setContactPerson(person);
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(179));
            parel.setName("Parel Leukemie, Myeloom, Lymfeklierkanker");
            parel.setAcronym("Parel LML");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Leukemie-myeloom-lymfeklierkanker"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to facilitate the rapid"
                    + " introduction of new diagnostics, treatments and"
                    + " therapies tailored to the needs of the individual"
                    + " patient, according to the genetic characteristics of"
                    + " their particular tumour. In the Netherlands, about 1500"
                    + " people a year are diagnosed with leukaemia and more"
                    + " than 1100 die of the condition.");
            parel.setMaterials(asSet(Arrays.asList(Material.CDNA,
                    Material.WHOLE_BLOOD, Material.DNA, Material.SERUM,
                    Material.OTHER)));
            parel.setNumberOfDonors(1060);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setContactPerson(person);
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(180));
            parel.setName("Parel Neurodegeneratieve hersenziekten");
            parel.setAcronym("Parel NDZ");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Neurodegeneratieve-hersenziekten"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.getDataCategories().add(DataCategory.IMAGING_DATA);
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Klinische data, inclusief beelden");
            parel.setMaterials(asSet(Arrays.asList(Material.SERUM, Material.PLASMA,
                    Material.OTHER, Material.WHOLE_BLOOD, Material.DNA)));
            parel.setNumberOfDonors(1065);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(181));
            parel.setName("Parel Nierfalen");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Nierfalen"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to better understand early forms"
                    + " of renal failure, affecting some 1650 patients each"
                    + " year, so as to improve prevention of its end stage and"
                    + " of the associated cardiovascular conditions.");
            parel.setMaterials(asSet(Arrays.asList(Material.DNA, Material.URINE, Material.SERUM)));
            parel.setNumberOfDonors(1901);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(182));
            parel.setName("Parel Reuma");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Reuma"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to prevent long-term invalidity"
                    + " for the 210,000 patients with rheumatoid arthritis and"
                    + " arthrosis.");
            parel.setMaterials(asSet(Arrays.asList(Material.PLASMA, Material.DNA,
                    Material.URINE, Material.SERUM)));
            parel.setNumberOfDonors(1581);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(183));
            parel.setName("Parel Multipele Endocriene Neoplasie ");
            parel.setAcronym("Parel MEN");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Multipele-Endocriene-Neoplasie-MEN"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setMaterials(asSet(Arrays.asList(Material.TISSUE_PARAFFIN_EMBEDDED)));
            parel.setNumberOfDonors(0);
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parel.setOmics(asSet(OmicsData.NAV));
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(184));
            parel.setName("Parel Ischemische hartziekten");
            parel.setAcronym("Parel IHZ");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Ischemische-hartziekten"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setMaterials(asSet(Arrays.asList(Material.DNA, Material.PLASMA, Material.SERUM)));
            parel.setNumberOfDonors(311);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(186));
            parel.setName("Parel Pancreas");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Pancreas"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Het doel van de PancreasParel is het"
                    + " verzamelen van pancreasweefsel en bloed bij elke"
                    + " electieve pancreasoperatie (ongeacht de indicatie)"
                    + " ten behoeve van wetenschappelijk onderzoek naar"
                    + " pancreas- en peri-ampullaire tumoren en chronische"
                    + " pancreatitis.");
            parel.setMaterials(asSet(Arrays.asList(Material.TISSUE_FROZEN,
                    Material.TISSUE_PARAFFIN_EMBEDDED, Material.DNA, Material.SERUM)));
            parel.setNumberOfDonors(0);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(115));
            parel.setName("Parel Inflammatoire darmziekten");
            parel.setAcronym("Parel IBD");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Inflammatoire-darmziekten-IBD"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setDescription("Objective is to better understand the causes"
                    + " of this group of conditions and the effectiveness of"
                    + " medicines and treatment for 60,000 patients with a"
                    + " chronic inflammation of the digestive tract.");
            parel.setMaterials(asSet(Arrays.asList(Material.TISSUE_PARAFFIN_EMBEDDED,
                    Material.DNA, Material.FECES, Material.SERUM, Material.PLASMA)));
            parel.setNumberOfDonors(4001);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(getPersons(parel));
            parel.setDiseases(diseases);
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(216));
            parel.setName("Parel Parkinson");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Parkinson"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setMaterials(asSet(Arrays.asList(Material.DNA, Material.PLASMA, Material.SERUM)));
            parel.setNumberOfDonors(0);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setDiseases(diseases);
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(asSet(personFactory("Henk", "Berendse", "h.berendse@vumc.nl")));
            parels.add(parel);

            parel = new SampleCollection();
            parel.setId(String.valueOf(217));
            parel.setName("Parel Oesofagus Maag tumoren");
            parel.setWebpage(new URL("http://www.parelsnoer.org/page/De-Parels/Oesofagus-Maag-tumoren"));
            parel.setSex(genders);
            parel.setDataCategories(new HashSet<>(data));
            parel.setTypes(asSet(CollectionType.DISEASE_SPECIFIC));
            parel.setInstitutes(organisations);
            parel.setMaterials(asSet(Arrays.asList(Material.PLASMA,
                    Material.SERUM, Material.DNA, Material.TISSUE_FROZEN,
                    Material.TISSUE_PARAFFIN_EMBEDDED)));
            parel.setNumberOfDonors(0);
            parel.setOmics(asSet(OmicsData.GENOMICS));
            parel.setDiseases(diseases);
            parel.setContactPerson(person);
            parel.setPrincipalInvestigators(asSet(personFactory("Jelle", "Ruurda", "j.p.ruurda@umcutrecht.nl")));
            parels.add(parel);

        } catch (MalformedURLException ex) {
            LOGGER.debug("Failed to set URL", ex);
        }
        return parels;
    }

    private Person personFactory(final String first, final String last, final String email) {
        Person person = new Person();
        person.setFirstName(first);
        person.setLastName(last);
        person.setEmail(email);
        Country NL = new Country();
        NL.setId("NL");
        NL.setName("The Netherlands");
        person.setCountry(NL);
        person.setId(String.valueOf(email.hashCode()));
        return person;
    }

    private Person personFactory(final String email) {
        Person person = new Person();
        person.setEmail(email);
        Country NL = new Country();
        NL.setId("NL");
        NL.setName("The Netherlands");
        person.setCountry(NL);
        person.setId(String.valueOf(email.hashCode()));
        return person;
    }

    private CollectionType getCollectionType(final SampleCollection sc) {
        Object[] args = {Integer.parseInt(sc.getId())};
        int[] types = {Types.INTEGER};
        List<String> result = jdbc.query("select o.name"
                + " from OntologyTerm as o"
                + " inner join BiobankSubCategory as c on o.id = c.id"
                + " inner join Biobank as b on b.SubCategory = c.id"
                + " where b.id = ?",
                args, types,
                (ResultSet rs, int rowNum) -> {
                    return rs.getString("name");
                });
        if (result.size() == 1) {
            String category = result.get(0).toLowerCase();

            if (category.startsWith("population")) {
                return CollectionType.POPULATION_BASED;
            } else if (category.startsWith("pathology")) {
                return CollectionType.HOSPITAL;
            }
        }
        return CollectionType.DISEASE_SPECIFIC;
    }

    private int normalizeNumber(String value) {
        int numberOfDonors = 0;
        try {
            if (value != null) {
                numberOfDonors = Integer.parseInt(value.replaceAll("[.,]", ""));
            }
        } catch (final NumberFormatException e) {
            LOGGER.info("Failed to parse PanelSize {}", value);
        }
        return numberOfDonors;
    }

    private <T> Set<T> asSet(T value) {
        Set<T> set = new HashSet<>();
        set.add(value);
        return set;
    }

    private <T> Set<T> asSet(Collection<T> values) {
        Set<T> set = new HashSet<>();
        set.addAll(values);
        return set;
    }

    private Set<OntologyTerm> getDiseaseTypes(Set<OntologyTerm> topics) {
        Set<OntologyTerm> types = new HashSet<>();
        OntologyTerm ni = new OntologyTerm();
        ni.setId("NI");
        topics.stream().forEach((disease) -> {
            types.addAll(dataMapper.getDiseaseTypeMapping(disease.getId()));
        });
        if (types.isEmpty()) {
            types.add(ni);
        }
        return types;
    }

    private Set<Material> getMaterialTypes(final SampleCollection sc) {
        Object[] args = {Integer.parseInt(sc.getId())};
        int[] types = {Types.INTEGER};
        List<Set<Material>> results = jdbc.query(
                "select distinct OntologyTerm.id from OntologyTerm"
                + " inner join Biobank_Biodata on Biobank_Biodata.Biodata = OntologyTerm.id"
                + " inner join Biobank on Biobank_Biodata.Biobank = Biobank.id"
                + " where Biobank.id = ?;",
                args, types,
                (ResultSet rs, int rowNum) -> {
                    return dataMapper.getMaterialsMapping(rs.getString("OntologyTerm.id"));
                });
        results.removeIf(type -> type == null);
        Set<Material> result = new HashSet<>();
        results.stream().forEach(result::addAll);
        if (result.isEmpty()) {
            result.add(Material.NAV);
        }
        return result;
    }

    private Set<OntologyTerm> getTopics(SampleCollection sc) {
        Object[] args = {Integer.parseInt(sc.getId())};
        int[] types = {Types.INTEGER};
        List<OntologyTerm> topics = jdbc.query("select OntologyTerm.* from OntologyTerm"
                + " inner join Biobank_Topic on OntologyTerm.id = Biobank_Topic.Topic"
                + " inner join Biobank on Biobank_Topic.Biobank = Biobank.id"
                + " where Biobank.id = ?;", args, types,
                (ResultSet rs, int rowNum) -> {
                    OntologyTerm topic = new OntologyTerm();
                    topic.setId(Integer.toString(rs.getInt("id")));
                    topic.setDescripton(rs.getString("definition"));
                    topic.setLabel(rs.getString("name"));
                    topic.setOntology(rs.getString("ontology"));
                    topic.setUri(null);
                    return topic;
                });
        return new HashSet<>(topics);
    }

    private Set<Person> getPersons(SampleCollection sc) throws DataAccessException, NumberFormatException {
        Object[] args = {Integer.parseInt(sc.getId())};
        int[] types = {Types.INTEGER};
        List<Person> persons = jdbc.query("select * from person as p"
                + " inner join biobank_coordinator as bc on p.id = bc.Coordinator"
                + " where bc.biobank = ?",
                args, types, (ResultSet rs, int rowNum) -> {
                    Person person = new Person();
                    person.setAddress(rs.getString("Address"));
                    person.setCity(rs.getString("City"));
                    person.setCountry(getCountryFromName(rs.getString("Country")));
                    person.setDepartment(rs.getString("Department"));
                    person.setEmail(rs.getString("Email"));
                    person.setFirstName(rs.getString("FirstName"));
                    person.setId(Integer.toString(rs.getInt("id")));
                    person.setOrganisation(getOrganisation(rs.getInt("Affiliation")));
                    person.setLastName(rs.getString("LastName"));
                    // Not recorded
                    person.setOrcid(null);
                    Optional<String> phone = Optional.ofNullable(rs.getString("Phone"));
                    if (phone.isPresent()) {
                        person.setPhone(normalizePhoneNumber(phone.get()));
                    }
                    // Not recorded
                    person.setZip(null);
                    return person;
                });
        return persons.stream().filter(pi -> StringUtils.isNotBlank(pi.getId())).collect(Collectors.toSet());
    }

    private String normalizePhoneNumber(final String value) {
        String phone = value.replaceAll("[^0-9]*", "");
        if (!phone.startsWith("31")) {
            if (phone.startsWith("0")) {
                phone = phone.substring(1);
            }
            phone = "+31" + phone;
        } else {
            phone = "+" + phone;
        }
        return phone;
    }

    private Set<Organisation> getInstitutes(SampleCollection sc) {
        int id = Integer.parseInt(sc.getId());
        Object[] args = {id};
        int[] types = {Types.INTEGER};
        List<Organisation> organisations = jdbc.query(
                "select i.* from Institute as i"
                + " inner join Biobank_Institutes as bi on bi.Institutes=i.id"
                + " inner join Biobank as b on b.id = bi.Biobank"
                + " where b.id = ?",
                args, types,
                (ResultSet rs, int rowNum) -> {
                    Organisation organisation = new Organisation();
                    organisation.setId(Integer.toString(rs.getInt("i.id")));
                    organisation.setAddress(rs.getString("i.Address"));
                    organisation.setCity(rs.getString("i.City"));
                    organisation.setCountry(getCountryFromName(rs.getString("i.Country")));
                    organisation.setName(rs.getString("i.name"));
                    organisation.setZip(null);
                    return organisation;
                });
        return new HashSet<>(organisations);
    }

    private Organisation getOrganisation(int id) {
        if (id == 0) {
            return null;
        }
        Object[] args = {id};
        int[] types = {Types.INTEGER};
        return jdbc.queryForObject("select * from Institute where id = ?",
                args, types, (ResultSet rs, int rowNum) -> {
                    Organisation organisation = new Organisation();
                    organisation.setId(Integer.toString(rs.getInt("id")));
                    organisation.setAddress(rs.getString("Address"));
                    organisation.setCity(rs.getString("City"));
                    organisation.setCountry(getCountryFromName(rs.getString("Country")));
                    organisation.setName(rs.getString("name"));
                    organisation.setZip(null);
                    return organisation;
                });
    }

    /**
     * Get the normalized ISO
     *
     * @param name the name.
     * @return the normalized ISO 6133:2 Country. Defaults to The Netherlands.
     */
    private Country getCountryFromName(final String name) {
        Country country;
        if (name != null && "Germany".equalsIgnoreCase(name)) {
            country = new Country("DE", "Germany");
        } else {
            country = new Country("NL", "The Netherlands");
        }
        return country;
    }
}
