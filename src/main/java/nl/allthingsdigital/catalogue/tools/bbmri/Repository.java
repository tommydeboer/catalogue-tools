/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import nl.allthingsdigital.catalogue.tools.miabis.SampleCollection;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
public interface Repository {

    void write(final List<SampleCollection> collections, final Path outfile) throws IOException;

}
