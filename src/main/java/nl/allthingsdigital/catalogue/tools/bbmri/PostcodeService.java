/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.net.URI;
import java.util.Optional;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Component
public final class PostcodeService {

    @Value("${postcodeapi.key}")
    private String apiKey;
    private final HttpClient client;

    @Value("${postcodeapi.service:api.postcodeapi.nu}")
    private String service;


    public PostcodeService() {
        client = HttpClientBuilder.create().build();
    }

    public Optional<WGS84Coordinate> getCoordinates(final NLPostalcode postcode) throws Exception {
        HttpGet request = new HttpGet(new URI("http", service, "/" + postcode.getCode(), null));
        request.setHeader("Accept", "*/*");
        request.setHeader("Api-Key", apiKey);
        HttpResponse response = client.execute(request);
        String entity = EntityUtils.toString(response.getEntity());
        JSONObject result = new JSONObject(entity);
        if (result.getBoolean("success")) {
            result = result.getJSONObject("resource");
            double lattitude = result.getDouble("latitude");
            double longitude = result.getDouble("longitude");
            return Optional.of(new WGS84Coordinate(lattitude, longitude));
        }
        return Optional.empty();
    }
}
