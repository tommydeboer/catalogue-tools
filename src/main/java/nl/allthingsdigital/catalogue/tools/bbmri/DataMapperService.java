/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import nl.allthingsdigital.catalogue.tools.miabis.Biobank;
import nl.allthingsdigital.catalogue.tools.miabis.OntologyTerm;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.CollectionType;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Material;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service that maintains mappings for BBMRI-NL 1.0 to MIABIS 2.0 data types.
 * The mappings are read from a Excel file that contains two sheets.
 * <dl>
 * <dt>materials</dt>
 * <dd>Mapping of the BBMRI-NL 1.0 topic strings to MIABIS 2.0 Material Types</dd>
 * <dt>collection_types</dt>
 * <dd>Mapping of the BBMRI-NL 1.0 topic strings to MIABIS 2.0 Collection Types</dd>
 * </dl>
 * Each sheet has two columns, the first one has the topic literals, while the
 * second column has a comma separated list of MIABIS 2.0 data types.
 * @author david
 */
@Service
public class DataMapperService {

    @Value("${mappings:src/main/resources/mappings.xlsx}")
    File mappings;

    private final Map<String, Set<Material>> materialsMap;
    private final Map<String, Set<CollectionType>> collectionTypesMap;
    private final Map<String, Set<OntologyTerm>> diseaseTypesMap;
    private final Map<String, Set<Biobank>> biobanksMap;

    public DataMapperService() {
        materialsMap = new HashMap<>();
        collectionTypesMap = new HashMap<>();
        diseaseTypesMap = new HashMap<>();
        biobanksMap = new HashMap<>();
    }

    @PostConstruct
    private void readMappings() {
        try {
            final Workbook wb = WorkbookFactory.create(mappings);
            Sheet sheet = wb.getSheet("materials");
            if (sheet != null) {
                materialsMap.putAll(readMap(sheet, Material.class));
            }
            sheet = wb.getSheet("collection_types");
            if (sheet != null) {
                collectionTypesMap.putAll(readMap(sheet, CollectionType.class));
            }
            sheet = wb.getSheet("disease_types");
            if (sheet != null) {
                diseaseTypesMap.putAll(readMap(sheet));
            }
            sheet = wb.getSheet("biobanks");
            if (sheet != null) {
                biobanksMap.putAll(readBiobankMap(sheet));
            }
        } catch (final IOException | InvalidFormatException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Set<Material> getMaterialsMapping(final String data){
        return materialsMap.getOrDefault(data, new HashSet<>());
    }

    public Set<CollectionType> getCollectionTypeMapping(final String data) {
        return collectionTypesMap.getOrDefault(data, new HashSet<>());
    }

    public Set<OntologyTerm> getDiseaseTypeMapping(final String data) {
        return diseaseTypesMap.getOrDefault(data, new HashSet<>());
    }

    public Set<Biobank> getBiobankMapping(final String data) {
        return biobanksMap.getOrDefault(data, new HashSet<>());
    }

    private Map<String, Set<OntologyTerm>> readMap(final Sheet sheet) {
        final Map<String, Set<OntologyTerm>> map = new HashMap<>();
        for (int rownum = sheet.getTopRow(); rownum <= sheet.getLastRowNum(); rownum++) {
            final Row row = sheet.getRow(rownum);
            final Cell key = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            final Cell value = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            if (key != null && value != null) {
                final String[] data = value.getStringCellValue().split(",");
                final Set<OntologyTerm> values = new HashSet<>();
                for (String datum : data) {
                    final OntologyTerm term = new OntologyTerm();
                    term.setId(datum);
                    values.add(term);
                }
                map.put(Integer.toString((int) key.getNumericCellValue()), values);
            }
        }
        return map;
    }

    private<T extends Enum<T>> Map<String, Set<T>> readMap(final Sheet sheet, final Class<T> t) {
        final Map<String, Set<T>> map = new HashMap<>();
        for (int rownum = sheet.getTopRow(); rownum <= sheet.getLastRowNum(); rownum++) {
            final Row row = sheet.getRow(rownum);
            final Cell key = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            final Cell value = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            if (key != null && value != null) {
                final String[] data = value.getStringCellValue().split(",");
                final Set<T> values = new HashSet<>();
                for (String datum : data) {
                    values.add(T.valueOf(t, datum.trim()));
                }
                map.put(Integer.toString((int) key.getNumericCellValue()), values);
            }
        }
        return map;
    }

    private Map<String, Set<Biobank>> readBiobankMap(final Sheet sheet) {
        final Map<String, Set<Biobank>> map = new HashMap<>();
        for (int rownum = sheet.getTopRow(); rownum <= sheet.getLastRowNum(); rownum++) {
            final Row row = sheet.getRow(rownum);
            final Cell key = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            final Cell value = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            if (key != null && value != null) {
                final String[] ids = value.getStringCellValue().split(",");
                final Set<Biobank> biobanks = new HashSet<>();
                for (final String id : ids) {
                    Biobank biobank = new Biobank();
                    biobank.setId(id);
                    biobanks.add(biobank);
                }
                map.put(key.getStringCellValue(), biobanks);
            }
        }
        return map;
    }
}
