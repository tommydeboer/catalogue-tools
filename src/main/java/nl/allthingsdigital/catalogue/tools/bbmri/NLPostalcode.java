/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
public final class NLPostalcode {
    private final String code;

    public NLPostalcode(final String value) {
        if (value.matches("[1-9]\\d{3}\\s*[a-zA-Z]{2}")) {
            code = value.replaceAll("\\s*", "");
        } else {
            throw new RuntimeException("Invalid postal code.");
        }
    }

    public String getCode() {
        return code;
    }
}
