/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.catalogue.tools.bbmri;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import nl.allthingsdigital.catalogue.tools.ldif.Entry;
import nl.allthingsdigital.catalogue.tools.miabis.Biobank;
import nl.allthingsdigital.catalogue.tools.miabis.Person;
import nl.allthingsdigital.catalogue.tools.miabis.SampleCollection;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.CollectionType;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.DataCategory;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Gender;
import nl.allthingsdigital.catalogue.tools.miabis.valuesets.Material;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Component
public final class LDIFRepository implements Repository {

    /**
     * Logger instance to log messages for this class.
     */
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(LDIFRepository.class);

    private static final String DN_TEMPLATE = "biobankID=%s,c=%s,ou=biobanks,dc=directory,dc=bbmri-eric,dc=eu";
    private static final String ID_TEMPLATE = "bbmri-eric:ID:%s_%s";
    private static final String biobankID = "biobankID";
    private static final String biobankName = "biobankName";
    private static final String biobankJuridicalPerson = "biobankJuridicalPerson";
    private static final String biobankCountry = "biobankCountry";
    private static final String biobankSize = "biobankSize";
    private static final String biobankAvailableMaleSamplesData = "biobankAvailableMaleSamplesData";
    private static final String biobankAvailableFemaleSamplesData = "biobankAvailableFemaleSamplesData";
    private static final String biobankAvailableBiologicalSamples = "biobankAvailableBiologicalSamples";
    private static final String biobankAvailableSurveyData = "biobankAvailableSurveyData";
    private static final String biobankAvailableImagingData = "biobankAvailableImagingData";
    private static final String biobankAvailableMedicalRecords = "biobankAvailableMedicalRecords";
    private static final String biobankAvailableNationalRegistries = "biobankAvailableNationalRegistries";
    private static final String biobankAvailableGenealogicalRecords = "biobankAvailableGenealogicalRecords";
    private static final String biobankAvailablePhysioBiochemMeasurements = "biobankAvailablePhysioBiochemMeasurements";
    private static final String biobankAvailableOther = "biobankAvailableOther";
    private static final String biobankSampleAccessFee = "biobankSampleAccessFee";
    private static final String biobankSampleAccessJointProjects = "biobankSampleAccessJointProjects";
    private static final String biobankSampleAccessDescription = "biobankSampleAccessDescription";
    private static final String biobankDataAccessFee = "biobankDataAccessFee";
    private static final String biobankDataAccessJointProjects = "biobankDataAccessJointProjects";
    private static final String biobankDataAccessDescription = "biobankDataAccessDescription";
    private static final String biobankSampleAccessURI = "biobankSampleAccessURI";
    private static final String biobankDataAccessURI = "biobankDataAccessURI";
    private static final String biobankITSupportAvailable = "biobankITSupportAvailable";
    private static final String biobankITStaffSize = "biobankITStaffSize";
    private static final String biobankISAvailable = "biobankISAvailable";
    private static final String biobankHISAvailable = "biobankHISAvailable";

    @Value("${bbmri.country:NL}")
    private String country;

    @Autowired
    PostcodeService postCodeService;

    @Override
    public void write(final List<SampleCollection> collections, final Path outfile) throws IOException {
        try (final BufferedWriter out = Files.newBufferedWriter(outfile, StandardCharsets.UTF_8)) {
            collections.forEach(SC -> {
                try {
                    out.write(convert(SC).toString());
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            });
        }
    }

    private Entry convert(final SampleCollection sc) {
        final Entry entry;
        List<String> classes = new ArrayList<>();
        classes.add(STRUCTURAL_OBJECT_CLASS);
        if (sc.getTypes().contains(CollectionType.HOSPITAL)) {
            classes.add("biobankClinical");
            String[] aux = new String[classes.size()];
            entry = new Entry(getDn(sc), classes.toArray(aux));
            entry.add("diagnosisAvailable", "urn:miriam:icd:*");
        } else if (sc.getTypes().contains(CollectionType.POPULATION_BASED)) {
            classes.add("biobankPopulation");
            String[] aux = new String[classes.size()];
            entry = new Entry(getDn(sc), classes.toArray(aux));
        } else {
            classes.add("biobankResearchStudy");
            String[] aux = new String[classes.size()];
            entry = new Entry(getDn(sc), classes.toArray(aux));
        }
        entry.add(biobankID, getID(sc));
        entry.add(biobankCountry, country.toUpperCase());
        if (StringUtils.isBlank(sc.getName())) {
            entry.add(biobankName, "N/A");
        } else {
            entry.add(biobankName, sc.getName());
        }
        entry.add(biobankAvailableBiologicalSamples, hasDataCategory(sc, DataCategory.BIOLOGICAL_SAMPLES));
        entry.add(biobankAvailableFemaleSamplesData, hasGender(sc, Gender.FEMALE));
        entry.add(biobankAvailableGenealogicalRecords, hasDataCategory(sc, DataCategory.GENEALOGICAL_RECORDS));
        entry.add(biobankAvailableImagingData, hasDataCategory(sc, DataCategory.IMAGING_DATA));
        entry.add(biobankAvailableMaleSamplesData, hasGender(sc, Gender.MALE));
        entry.add(biobankAvailableMedicalRecords, hasDataCategory(sc, DataCategory.MEDICAL_RECORDS));
        entry.add(biobankAvailableNationalRegistries, hasDataCategory(sc, DataCategory.NATIONAL_REGISTRIES));
        entry.add(biobankAvailableOther, hasDataCategory(sc, DataCategory.OTHER));
        entry.add(biobankAvailablePhysioBiochemMeasurements, hasDataCategory(sc, DataCategory.PHYSIOLOGICAL_BIOCHEMICAL_MEASUREMENTS));
        entry.add(biobankAvailableSurveyData, hasDataCategory(sc, DataCategory.SURVEY_DATA));
        entry.add("biobankPartnerCharterSigned", "FALSE");
        Set<Biobank> biobanks = sc.getBiobanks();
        if (biobanks != null && !biobanks.isEmpty()) {
            biobanks.forEach(B -> entry.add(biobankJuridicalPerson, B.getName()));
        } else {
            entry.add(biobankJuridicalPerson, "unknown");
        }
        for (Material m : Material.values()) {
            if (m.getAttributeName() != null) {
                entry.add(m.getAttributeName(), hasMaterial(sc, m));
            }
        }
        if (sc.getNumberOfDonors() > 0) {
            entry.add(biobankSize, getBiobankSize(sc));
        }
        if (sc.getContactPerson() != null) {
            addContact(entry, sc.getContactPerson());
        }
        return entry;
    }
    private static final String STRUCTURAL_OBJECT_CLASS = "biobank";

    private String getBiobankSize(final SampleCollection sc) {
        Double size = Math.floor(Math.log10(sc.getNumberOfDonors()));
        return Integer.toString(size.intValue());
    }

    private String hasGender(final SampleCollection sc, final Gender g) {
        return Boolean.toString(sc.getSex().contains(g)).toUpperCase();
    }

    private String hasMaterial(final SampleCollection sc, final Material m) {
        return Boolean.toString(sc.getMaterials().contains(m)).toUpperCase();
    }

    private String hasDataCategory(final SampleCollection sc, final DataCategory category) {
        return Boolean.toString(sc.getDataCategories().contains(category)).toUpperCase();
    }

    private String getDn(final SampleCollection sc) {
        return String.format(DN_TEMPLATE, getID(sc), country.toLowerCase());
    }

    private String getID(final SampleCollection sc) {
        return String.format(ID_TEMPLATE, country.toUpperCase(), sc.getId());
    }

    private void addContact(final Entry entry, final Person person) {
        entry.add("biobankContactFirstName", person.getFirstName());
        entry.add("biobankContactLastName", person.getLastName());
        entry.add("biobankContactPhone", person.getPhone());
        entry.add("biobankContactEmail", person.getEmail());
        entry.add("biobankContactAddress", person.getAddress());
        entry.add("biobankContactZIP", person.getZip());
        entry.add("biobankContactCity", person.getCity());
        entry.add("biobankContactCountry", person.getCountry().getId());
        if (StringUtils.isNotBlank(person.getZip())) {
            try {
                NLPostalcode code = new NLPostalcode(person.getZip());
                Optional<WGS84Coordinate> coord = postCodeService.getCoordinates(code);
                if (coord.isPresent()) {
                    WGS84Coordinate wgs84 = coord.get();
                    entry.add("biobankContactLatitude", Double.toString(wgs84.getLatitude()));
                    entry.add("biobankContactLongitude", Double.toString(wgs84.getLongitude()));
                }
            } catch (final Exception ex) {
                LOGGER.info("failed to get coordinates: ", ex);
            }
        }
    }
}
